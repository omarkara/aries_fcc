import Link from "next/link";
import style from "./style.module.scss";
import { BsArrowLeft } from "react-icons/bs";

export default function Custom404page() {
  return (
    <section className={style.section}>
      <div>
        <h2>
          <span>oops..!</span>
          <span>page not found.</span>
        </h2>
        <p>The requested URL was not found.</p>
        <div>
          <BsArrowLeft />
          <Link href='/'>
            <a>Back to home</a>
          </Link>
        </div>
      </div>
      <h1>
        <span>4</span>
        <span>0</span>
        <span>4</span>
      </h1>
    </section>
  );
}
