import style from "./style.module.scss";

export default function PageMainImage({ Post }) {
  const JsonToJsx = (elements) => {
    return (
      <>
        {elements.map((ele) => (
          <ele.type key={ele.id} {...ele.attr}>
            {ele.innerText}
          </ele.type>
        ))}
      </>
    );
  };
  return (
    <section className={style.section}>
      <h1 className={style.title}>{Post.title}</h1>
      <div className={style.pageContent}>{JsonToJsx(Post.elements)}</div>
    </section>
  );
}
