import style from "./style.module.scss";
import Image from "next/image";

export default function PageMainImage() {
  return (
    <section className={style.section}>
      <h2>
        <span>insight, resources</span> and stories.
      </h2>
      <div>
        <Image
          priority
          layout='fill'
          objectFit='cover'
          src='/hero image1.png'
          alt='background girl image'
        />
      </div>
    </section>
  );
}
