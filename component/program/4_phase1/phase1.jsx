import style from "./style.module.scss";
import Image from "next/image";

const Phase1 = () => {
  return (
    <section className={style.section}>
      <span>phase 1</span>
      <h3>interative product development.</h3>
      <p>
        Teams transition into Phase 1 with clear milestones and projects for
        product and business development. Phase 1 emphasizes AI of prototyping,
        iteration and testing as well as building up networks for suppliers,
        experts and manufacturers moving towards production.
      </p>
      <div className={style.Images}>
        <div className={style.first}>
          <Image
            src='/image-small1.png'
            layout='fill'
            objectFit='cover'
            alt='small_image'
          />
        </div>
        <div className={style.secund}></div>
        <div className={style.third}>
          <Image
            src='/image-big1.png'
            layout='fill'
            objectFit='cover'
            alt='small_image'
          />
        </div>
      </div>
    </section>
  );
};

export default Phase1;
