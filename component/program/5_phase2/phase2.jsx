import style from "./style.module.scss";
import Image from "next/image";

const Phase2 = () => {
  return (
    <section className={style.section}>
      <span>phase 2</span>
      <h3>growth & fundraising.</h3>
      <p>
        Founders prime for scaling through expanded business development,
        manufacturing, and concentrated venture capital fundraising. We closely
        collaborate with every team as they build out their investment strategy
        and support with our network of the world’s top investors aligned with
        physical / digital technology investment, along with our own investment
        through PIXIRY.
      </p>
      <div className={style.Images}>
        <div className={style.first}>
          <Image
            src='/image-small2.png'
            layout='fill'
            objectFit='cover'
            alt='small_image'
          />
        </div>
        <div className={style.secund}></div>
        <div className={style.third}>
          <Image
            src='/image-big2.png'
            layout='fill'
            objectFit='cover'
            alt='small_image'
          />
        </div>
      </div>
    </section>
  );
};

export default Phase2;
