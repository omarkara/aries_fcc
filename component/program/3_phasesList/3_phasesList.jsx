import style from "./style.module.scss";
import line from "../../../styles/line.module.scss";

const PhasesList = () => {
  return (
    <section className={style.section}>
      <ul>
        <li>
          <span>PHASE 1 (0 - 120 DAYS)</span>
          <div className={line.line}></div>
          <p>rapid product development.</p>
        </li>
        <li>
          <span>PHASE 2 (120 - 180 DAYS)</span>
          <div className={line.line}></div>
          <p>growth & fundraising.</p>
        </li>
        <li>
          <span>PHASE 1 (180 + DAYS)</span>
          <div className={line.line}></div>
          <p>onword support & community.</p>
        </li>
      </ul>
    </section>
  );
};

export default PhasesList;
