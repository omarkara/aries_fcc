import style from "./style.module.scss";
import Image from "next/image";
export default function BuildingFaster() {
  return (
    <section className={style.section}>
      <h1>
        <span>Building faster,</span> smarter, together.
      </h1>
      <div>
        <Image
          priority
          layout='fill'
          objectFit='cover'
          src='/big-centered image.png'
          alt='background girl image'
        />
      </div>
    </section>
  );
}
