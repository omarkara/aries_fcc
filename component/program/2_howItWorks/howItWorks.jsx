import style from "./style.module.scss";
import line from "../../../styles/line.module.scss";

const HowItWorks = () => {
  return (
    <section className={style.section}>
      <p className={style.title}>AI Program</p>
      <h2>how the program works.</h2>
      <span className={line.line}></span>
      <p>
        The AI Program combines a $250,000 USD investment, 180 days of hands-on
        collaboration, and a global founder community for early stage founders
        building hard-tech startups. Founders work with us across our global
        locations to collaborate with our in-house team of engineers, designers
        and entrepreneurs. Startups get access to the unparalleled experts,
        investors, manufacturers and supply chains to thrive as an early stage
        hard tech startup.
      </p>
      <p>
        We break the 180 days into 3 phases that help us rapidly scale our
        collaboration. This starts with a deep dive, and quickly ramps to
        hands-on engagement both remotely and in our offices around the world.
      </p>
    </section>
  );
};

export default HowItWorks;
