import style from "./style.module.scss";
import Image from "next/image";

const Phase3 = () => {
  return (
    <section className={style.section}>
      <span>phase 3</span>
      <h3>community onboarding.</h3>
      <p>
        After the program, startups still work closely with ARIES leadership,
        investment team and business development team throughout their startup
        journey. While we don't continue to provide engineering and product
        development support, we are part of your company for the long haul and
        want to see you succeed.
      </p>
      <div className={style.Images}>
        <div className={style.first}>
          <Image
            src='/image-small3.png'
            layout='fill'
            objectFit='cover'
            alt='small_image'
          />
        </div>
        <div className={style.secund}></div>
        <div className={style.third}>
          <Image
            src='/image-big3.png'
            layout='fill'
            objectFit='cover'
            alt='small_image'
          />
        </div>
      </div>
    </section>
  );
};

export default Phase3;
