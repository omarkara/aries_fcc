import style from "./style.module.scss";
import Image from "next/image";
import Link from "next/link";
const Solution = () => {
  const activate = (e) => {
    if (e.target.id === "active") {
      e.target.id = "";
    } else e.target.id = "active";
  };
  return (
    <section className={style.section}>
      <div className={style.firstP}>
        <h2>arise Funding solution for Tech Startups.</h2>
        <p>
          They are showing researches and writting regularly about the
          consequnces happening on AI. Our unique program is designed for
          startups, combining hands-on help from a deeply experienced product
          development team and a total investment package of $270,000.
        </p>
      </div>
      <ul className={style.filter}>
        <li onClick={activate}>Reactive Machines</li>
        <li onClick={activate}>limited memory</li>
        <li onClick={activate}>Self-aware</li>
        <li onClick={activate}>Theory of Mind</li>
      </ul>
      <ul className={style.cards}>
        <li>
          <div className={style.image}>
            <Image
              src='/image1.png'
              layout='fill'
              objectFit='cover'
              alt='image 1'
            />
          </div>
          <div className={style.content}>
            <span>2016</span>
            <h4>Btlr.</h4>
            <p>
              Personalized sound that adapts to your individual hearing to
              deliver a revolutionary experience.
            </p>
            <Link href='/'>
              <a>
                View program
                <div>
                  <Image
                    src='/Arrow 2.svg'
                    layout='fill'
                    objectFit='contain'
                    alt='arrow'
                  />
                </div>
              </a>
            </Link>
          </div>
        </li>
        <li>
          <div className={style.image}>
            <Image
              src='/maximalfocus-naSAHDWRNbQ-unsplash 1.png'
              layout='fill'
              objectFit='cover'
              alt='image 1'
            />
          </div>
          <div className={style.content}>
            <span>2014</span>
            <h4>nura.</h4>
            <p>
              Personalized sound that adapts to your individual hearing to
              deliver a revolutionary experience.
            </p>
            <Link href='/'>
              <a>
                View program
                <div>
                  <Image
                    src='/Arrow 2.svg'
                    layout='fill'
                    objectFit='contain'
                    alt='arrow'
                  />
                </div>
              </a>
            </Link>
          </div>
        </li>
        <li>
          <div className={style.image}>
            <Image
              src='/image3.png'
              layout='fill'
              objectFit='cover'
              alt='image 1'
            />
          </div>
          <div className={style.content}>
            <span>2018</span>
            <h4>pixi.</h4>
            <p>
              Personalized sound that adapts to your individual hearing to
              deliver a revolutionary experience.
            </p>
            <Link href='/'>
              <a>
                View program
                <div>
                  <Image
                    src='/Arrow 2.svg'
                    layout='fill'
                    objectFit='contain'
                    alt='arrow'
                  />
                </div>
              </a>
            </Link>
          </div>
        </li>
      </ul>
      <div className={style.arrows}>
        <div>
          <Image
            src='/prev arrow.svg'
            layout='fill'
            objectFit='contain'
            alt='arrow left'
          />
        </div>
        <div>
          <Image
            src='/next arrow.svg'
            layout='fill'
            objectFit='contain'
            alt='arrow right'
          />
        </div>
      </div>
    </section>
  );
};

export default Solution;
