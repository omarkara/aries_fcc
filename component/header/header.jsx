import Image from "next/image";
import style from "./style.module.scss";
import Link from "next/link";
import { useRef } from "react";
import MoreLinks from "./MoreLinks/MoreLinks";

export default function Header() {
  const MoreLinksRef = useRef(null);
  const handleClick = () => {
    if (MoreLinksRef.current.id === "active") {
      MoreLinksRef.current.id = "";
    } else {
      MoreLinksRef.current.id = "active";
    }
  };

  return (
    <>
      <MoreLinks MoreLinksRef={MoreLinksRef} />
      <header className={style.header}>
        <div className={style.Ul}>
          <ul>
            <li>
              <Link href='/program'>
                <a>PROGRAM</a>
              </Link>
            </li>
            <li>
              <Link href='/funding'>
                <a>FUNDING</a>
              </Link>
            </li>
          </ul>
        </div>
        <div>
          <Link href='/'>
            <a>
              <Image
                width='138px'
                height='40px'
                src='/logo.svg'
                alt='aries logo'
              />
            </a>
          </Link>
        </div>
        <div style={{ display: "flex", gap: "5px", alignItems: "center" }}>
          <ul className={style.Ul}>
            <li>
              <Link href='/jobs'>
                <a>JOBS</a>
              </Link>
            </li>
            <li onClick={handleClick}>
              <button>MENU</button>
            </li>
          </ul>
          <div
            className={style.logoImage}
            onClick={handleClick}
            style={{ cursor: "pointer" }}
          >
            <Image
              layout='fill'
              objectFit='cover'
              src='/MenuIcon.svg'
              alt='list icon'
            />
          </div>
        </div>
      </header>
    </>
  );
}
