import style from "./style.module.scss";
import Link from "next/link";

function MoreLinks({ MoreLinksRef }) {
  return (
    <div ref={MoreLinksRef} className={style.MoreLinks}>
      <ul>
        <li>
          <Link href='/blog'>
            <a>BLOG</a>
          </Link>
        </li>
        <li>
          <Link href='/blog/topics/'>
            <a>all posts</a>
          </Link>
        </li>
        <li>
          <Link href='/blog/topics/founder stories'>
            <a>founder stories</a>
          </Link>
        </li>
        <li>
          <Link href='/blog/topics/aries news'>
            <a>aries news</a>
          </Link>
        </li>
        <li>
          <Link href='/blog/topics/aries toolkit'>
            <a>aries toolkit</a>
          </Link>
        </li>
        <li>
          <Link href='/blog/topics/portfolio news'>
            <a>portfolio news</a>
          </Link>
        </li>
        <li>
          <Link href='/blog/topics/trend & insights'>
            <a>trend & insights</a>
          </Link>
        </li>
      </ul>
      <ul>
        <li>
          <Link href='/'>
            <a>company</a>
          </Link>
        </li>
        <li>
          <Link href='/program'>
            <a>PROGRAM</a>
          </Link>
        </li>
        <li>
          <Link href='/funding'>
            <a>FUNDING</a>
          </Link>
        </li>
        <li>
          <Link href='/jobs'>
            <a>JOBS</a>
          </Link>
        </li>
        <li>
          <Link href='/team'>
            <a>team</a>
          </Link>
        </li>
      </ul>
      <ul>
        <li>more pages</li>
        <li>
          <Link href='/Privacy Statement'>
            <a>Privacy Statement</a>
          </Link>
        </li>
        <li>
          <Link href='/cookies policy'>
            <a>cookies policy</a>
          </Link>
        </li>
      </ul>
      <span
        className={style.close}
        onClick={() => {
          MoreLinksRef.current.id = "";
        }}
      >
        X
      </span>
    </div>
  );
}

export default MoreLinks;
