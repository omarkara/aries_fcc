import Link from "next/link";
import style from "./style.module.scss";
import { BsArrowRight } from "react-icons/bs";

export default function Footer() {
  return (
    <footer className={style.footer}>
      <div className={style.weHelp}>
        <div>
          <h2>we help you to turn ideas into reality.</h2>
          <p>
            if your research processes have unique features, and you are looking
            for an individual funding solution - we can help.
          </p>
        </div>
        <form>
          <div>
            <input required type='text' title='ادخل اسمك هنا' id='Name' />
            <label htmlFor='Name'>name*</label>
          </div>
          <div>
            <input required type='email' title='ادخل إيميلك' id='Email' />
            <label htmlFor='Email'>email*</label>
          </div>
          <div>
            <input required type='text' id='Message' />
            <label htmlFor='Message'>message*</label>
          </div>

          <span>
            <input type='submit' value='get in touch' id='submit' />
            <BsArrowRight />
          </span>
        </form>
      </div>
      <ul className={style.socialMedia}>
        <li>
          <Link href='/'>
            <a style={{ color: "#99a9ff" }}>LINKEDIN </a>
          </Link>
          <Link href='/'>
            <a>reddit </a>
          </Link>
          <Link href='/'>
            <a>facebook </a>
          </Link>
          <Link href='/'>
            <a>twitter </a>
          </Link>
        </li>
        <li>
          <Link href='/'>
            <a>privacy & policy </a>
          </Link>
          <Link href='/'>
            <a> cookies policy </a>
          </Link>
        </li>
      </ul>
      <div className={style.allRights}>
        <span className={style.line}></span>
        <p>
          all rights reserved by <span>PIXIRHY</span>
        </p>
      </div>
    </footer>
  );
}
