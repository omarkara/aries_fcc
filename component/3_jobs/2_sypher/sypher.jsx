import style from "./style.module.scss";

export default function Sypher() {
  return (
    <section className={style.section}>
      <div className={style.titleSave}>
        <div className={style.title}>
          <span>CYPHER</span>
          <h2>
            <span>head of growth -</span> medical device startup.
          </h2>
        </div>
        <div className={style.saveShare}>
          <span>
            <img src='/saveIcon.svg' alt='save Icon' />
            save
          </span>
          <span>
            <img src='/shareIcon.svg' alt='share   Icon' />
            share
          </span>
        </div>
      </div>
      <div className={style.dateWriter}>
        <span>Silicon Valley - California, USA</span>
        <span>Posted 7 days ago - 72 applicants</span>
      </div>
    </section>
  );
}
