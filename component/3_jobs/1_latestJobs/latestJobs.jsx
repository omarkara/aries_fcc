import style from "./style.module.scss";
import Image from "next/image";

export default function LatestJobs() {
  return (
    <section className={style.section}>
      <h1>
        <span>Latest </span> Portfolio Jobs.
      </h1>
      <div>
        <Image
          priority
          layout='fill'
          objectFit='cover'
          src='/hero image.png'
          alt='background girl image'
        />
      </div>
    </section>
  );
}
