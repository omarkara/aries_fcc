import style from "./style.module.scss";

export default function Requirements() {
  return (
    <section className={style.section}>
      <ul>
        <li>
          <div>
            <h4>experience</h4>
            <p>minimum 1 year</p>
          </div>
        </li>
        <li>
          <div>
            <h4>work level</h4>
            <p>senior level</p>
          </div>
        </li>
        <li>
          <div>
            <h4>employee type</h4>
            <p>full time job</p>
          </div>
        </li>
        <li>
          <div>
            <h4>offer salary</h4>
            <p>$2750.0 / month</p>
          </div>
        </li>
      </ul>
    </section>
  );
}
