import style from "./style.module.scss";
import Link from "next/link";
import Image from "next/image";
export default function JobDetails() {
  return (
    <section className={style.section}>
      <h4>overview</h4>
      <p>
        Deeply understand our customers, our product, and our business
        strategies. Together with CEO and small team establish strategy and OKRs
        for all growth marketing programs and monitor investments to deliver
        improved ROI performance. Owning and driving budgets across the channel
        mix.
      </p>
      <h4>job description</h4>
      <p>We are now looking for our Head of Growth.</p>
      <p>
        This position is for you, if you: are excited to bring a new treatments
        to the world for diseases that affect billions of people worldwide. want
        to work in the intersection of medical technology, digital health,
        hardware, psychology and artificial intelligence.
      </p>
      <p>
        have a strong passion and integrity for what you do and want to
        challenge the way we treat mental health problems today. enjoy being one
        of the first employees in a startup with truly global potential and
        ambitions, and be a part of shaping the culture and future of the
        company. are a natural leader that will scale with the organisation as
        it grows.
      </p>
      <h4>Responsibilities</h4>
      <p>
        Deeply understand our customers, our product, and our business
        strategies. Together with CEO and small team establish strategy and OKRs
        for all growth marketing programs and monitor investments to deliver
        improved ROI performance. Owning and driving budgets across the channel
        mix.
      </p>
      <p>
        Deeply understand our customers, our product, and our business
        strategies. Together with CEO and small team establish strategy and OKRs
        for all growth marketing programs and monitor investments to deliver
        improved ROI performance. Owning and driving budgets across the channel
        mix.
      </p>
      <div className={style.skills}>
        <h4>skills</h4>
        <ul>
          <li>
            <p>Creative</p>
          </li>
          <li>
            <p>Integrity</p>
          </li>
          <li>
            <p>Intellectual breadth</p>
          </li>
          <li>
            <p>Ability to admit when wrong</p>
          </li>
          <li>
            <p>Ability to work in a diverse team</p>
          </li>
          <li>
            <p>Sense of humor</p>
          </li>
          <li>
            <p>Emotional openness</p>
          </li>
        </ul>
      </div>
      <Link href='path'>
        <a>
          apply to job
          <div>
            <Image
              src='/Arrow 2.svg'
              layout='fill'
              objectFit='cover'
              alt='arrow svg icon'
            />
          </div>
        </a>
      </Link>
    </section>
  );
}
