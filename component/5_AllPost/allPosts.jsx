import style from "./style.module.scss";
import Image from "next/image";
import Link from "next/link";

export default function AllPosts({ posts }) {
  return (
    <section className={style.section}>
      <ul>
        {posts.map((post) => {
          return (
            <li key={post.id}>
              <div className={style.image}>
                <Image
                  src={"/" + post.image}
                  layout='fill'
                  objectFit='cover'
                  alt='post image'
                />
              </div>
              <Link href={"/blog/" + post.title}>
                <a>
                  <div className={style.title}>
                    <span>{post.writingData}</span>
                    <h3>{post.title}</h3>
                  </div>
                </a>
              </Link>
            </li>
          );
        })}
      </ul>
    </section>
  );
}
