import style from "./style.module.scss";
import Image from "next/image";
import Link from "next/link";
import { useRef } from "react";

const status = {
  onTheObject: false,
  down: false,
  initialClientX: 0,
  initialPosition: -200,
  width: 0,
};
export default function Trends3() {
  const Ul = useRef(null);
  const handleMouseDown = (e) => {
    if (window.innerWidth < 800) {
      return;
    }
    e.preventDefault();
    status.down = true;
    status.onTheObject = true;
    status.initialClientX = e.clientX;
    status.width = Ul.current.clientWidth;
    Ul.current.style.cursor = "grab";
  };
  const handleMouseUp = () => {
    status.down = false;
    Ul.current.style.cursor = "initial";
  };
  const handleMoving = (e) => {
    const { clientX, movementX } = e;
    const moveTheUl = () => {
      const oldPosition = status.initialPosition;
      const newPosition = oldPosition + movementX;

      if (
        newPosition <
          -(status.width - window.innerWidth) - window.innerWidth / 5 ||
        newPosition > 0
      ) {
        return;
      }

      Ul.current.style.left = newPosition + "px";
      status.initialPosition = newPosition;
    };

    status.down && movementX && status.onTheObject && moveTheUl();
  };
  return (
    <section className={style.section}>
      <span>trends & insights.</span>
      <ul
        ref={Ul}
        onMouseDown={handleMouseDown}
        onMouseMove={handleMoving}
        onMouseUp={handleMouseUp}
        onMouseLeave={() => (status.onTheObject = false)}
      >
        <li>
          <div className={style.image}>
            <Image
              src='/post2.png'
              layout='fill'
              objectFit='cover'
              alt='post image'
            />
          </div>
          <Link href='/blog/vertical Farming at it’s highest. Nothing has grown like this in decades.'>
            <a>
              <div className={style.title}>
                <span>07-09-2021</span>
                <h3>
                  vertical Farming at it’s highest. Nothing has grown like this
                  in decades.
                </h3>
              </div>
            </a>
          </Link>
        </li>
        <li>
          <div className={style.image}>
            <Image
              src='/post3.png'
              layout='fill'
              objectFit='cover'
              alt='post image'
            />
          </div>
          <Link href='/blog/They are saying this is the future of bio-tech.'>
            <a>
              <div className={style.title}>
                <span>10-08-2021</span>
                <h3>They are saying this is the future of bio-tech.</h3>
              </div>
            </a>
          </Link>
        </li>
        <li>
          <div className={style.image}>
            <Image
              src='/post4.png'
              layout='fill'
              objectFit='cover'
              alt='post image'
            />
          </div>
          <Link href='/blog/Sound of future & noise cancelling of the decade.'>
            <a>
              <div className={style.title}>
                <span>10-08-2021</span>
                <h3>Sound of future & noise cancelling of the decade.</h3>
              </div>
            </a>
          </Link>
        </li>
      </ul>
    </section>
  );
}
