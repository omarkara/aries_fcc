import style from "./style.module.scss";
import Image from "next/image";
import Link from "next/link";
import { useRef } from "react";

const status = {
  onTheObject: false,
  down: false,
  initialClientX: 0,
  initialPosition: -200,
  width: 0,
};
export default function AriesToolkit() {
  const Ul = useRef(null);
  const handleMouseDown = (e) => {
    if (window.innerWidth < 800) {
      return;
    }
    e.preventDefault();
    status.down = true;
    status.onTheObject = true;
    status.initialClientX = e.clientX;
    status.width = Ul.current.clientWidth;
    Ul.current.style.cursor = "grab";
  };
  const handleMouseUp = () => {
    status.down = false;
    Ul.current.style.cursor = "initial";
  };
  const handleMoving = (e) => {
    const { clientX, movementX } = e;
    const moveTheUl = () => {
      const oldPosition = status.initialPosition;
      const newPosition = oldPosition + movementX;

      if (
        newPosition <
          -(status.width - window.innerWidth) - window.innerWidth / 5 ||
        newPosition > 0
      ) {
        return;
      }

      Ul.current.style.left = newPosition + "px";
      status.initialPosition = newPosition;
    };

    status.down && movementX && status.onTheObject && moveTheUl();
  };
  return (
    <section className={style.section}>
      <span>aries toolkit.</span>
      <ul
        ref={Ul}
        onMouseDown={handleMouseDown}
        onMouseMove={handleMoving}
        onMouseUp={handleMouseUp}
        onMouseLeave={() => (status.onTheObject = false)}
      >
        <li>
          <div className={style.image}>
            <Image
              src='/post8.png'
              layout='fill'
              objectFit='cover'
              alt='post image'
            />
          </div>
          <Link href='/blog/We are thinking of launching this kit for you.'>
            <a>
              <div className={style.title}>
                <span>10-08-2021</span>
                <h3>We are thinkin of launching this kit for you.</h3>
              </div>
            </a>
          </Link>
        </li>
        <li>
          <div className={style.image}>
            <Image
              src='/post9.png'
              layout='fill'
              objectFit='cover'
              alt='post image'
            />
          </div>
          <Link href='/blog/Is this kit any good for the people of japan or indonesia.'>
            <a>
              <div className={style.title}>
                <span>07-09-2021</span>
                <h3>
                  Is this kit any good for the people of japan or indonesia?
                  drop.
                </h3>
              </div>
            </a>
          </Link>
        </li>
        <li>
          <div className={style.image}>
            <Image
              src='/post10.png'
              layout='fill'
              objectFit='cover'
              alt='post image'
            />
          </div>
          <Link href='/blog/clean yet minimal kit for all the business people in Air.'>
            <a>
              <div className={style.title}>
                <span>10-08-2021</span>
                <h3>
                  clean yet minimal kit for all the business peope in Air.
                </h3>
              </div>
            </a>
          </Link>
        </li>
      </ul>
    </section>
  );
}
