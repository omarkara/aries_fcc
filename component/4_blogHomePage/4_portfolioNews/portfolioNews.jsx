import style from "./style.module.scss";
import Image from "next/image";
import Link from "next/link";
import { useRef } from "react";

const status = {
  onTheObject: false,
  down: false,
  initialClientX: 0,
  initialPosition: -200,
  width: 0,
};
export default function PortfolioNews() {
  const Ul = useRef(null);
  const handleMouseDown = (e) => {
    if (window.innerWidth < 800) {
      return;
    }
    e.preventDefault();
    status.down = true;
    status.onTheObject = true;
    status.initialClientX = e.clientX;
    status.width = Ul.current.clientWidth;
    Ul.current.style.cursor = "grab";
  };
  const handleMouseUp = () => {
    status.down = false;
    Ul.current.style.cursor = "initial";
  };
  const handleMoving = (e) => {
    const { clientX, movementX } = e;
    const moveTheUl = () => {
      const oldPosition = status.initialPosition;
      const newPosition = oldPosition + movementX;

      if (
        newPosition <
          -(status.width - window.innerWidth) - window.innerWidth / 5 ||
        newPosition > 0
      ) {
        return;
      }

      Ul.current.style.left = newPosition + "px";
      status.initialPosition = newPosition;
    };

    status.down && movementX && status.onTheObject && moveTheUl();
  };
  return (
    <section className={style.section}>
      <span>portfolio news.</span>
      <ul
        ref={Ul}
        onMouseDown={handleMouseDown}
        onMouseMove={handleMoving}
        onMouseUp={handleMouseUp}
        onMouseLeave={() => (status.onTheObject = false)}
      >
        <li>
          <div className={style.image}>
            <Image
              src='/post5.png'
              layout='fill'
              objectFit='cover'
              alt='post image'
            />
          </div>
          <Link href='/blog/Our $250m investment in Virealm is returning crazily.'>
            <a>
              <div className={style.title}>
                <span>10-08-2021</span>
                <h3>Our $250m investment in Virealm is returning crazily.</h3>
              </div>
            </a>
          </Link>
        </li>
        <li>
          <div className={style.image}>
            <Image
              src='/post6.png'
              layout='fill'
              objectFit='cover'
              alt='post image'
            />
          </div>
          <Link href='/blog/Was our 30 Billion Dollar Investment wise on this tech.'>
            <a>
              <div className={style.title}>
                <span>10-08-2021</span>
                <h3>
                  Was our 30 Billion Dollar Investement wise on this tech?
                </h3>
              </div>
            </a>
          </Link>
        </li>
        <li>
          <div className={style.image}>
            <Image
              src='/post7.png'
              layout='fill'
              objectFit='cover'
              alt='post image'
            />
          </div>
          <Link href='/blog/Chips we believed in is nothing but excellence in quality'>
            <a>
              <div className={style.title}>
                <span>07-09-2021</span>
                <h3>
                  Chips we believed in is nothing but excellence in quality
                </h3>
              </div>
            </a>
          </Link>
        </li>
      </ul>
    </section>
  );
}
