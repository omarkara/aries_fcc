import style from "./style.module.scss";
import Image from "next/image";
import Link from "next/link";
export default function Trends() {
  return (
    <section className={style.section}>
      <h1>trends and insights.</h1>
      <div className={style.content}>
        <div className={style.post}>
          <Link href='/blog/announcing the aries venture fellowship.'>
            <a>
              <span>featured pots.</span>
              <div>
                <Image
                  src='/post1.png'
                  layout='fill'
                  objectFit='cover'
                  alt='light out of the tunnel'
                />
              </div>
              <h2>
                <span>07-09-2021</span>announcing the aries venture fellowship.
              </h2>
            </a>
          </Link>
        </div>
        <div className={style.topics}>
          <h2>insight topics</h2>
          <ul>
            <li>
              <Link href='/blog/topics/'>
                <a>
                  all posts
                  <div>
                    <Image
                      src='/arrow 2.svg'
                      layout='fill'
                      objectFit='cover'
                      alt='arrow icon'
                    />
                  </div>
                </a>
              </Link>
            </li>
            <li>
              <Link href='/blog/topics/founder stories'>
                <a>
                  founder stories
                  <div>
                    <Image
                      src='/arrow 2.svg'
                      layout='fill'
                      objectFit='cover'
                      alt='arrow icon'
                    />
                  </div>
                </a>
              </Link>
            </li>
            <li>
              <Link href='/blog/topics/aries news'>
                <a>
                  aries news
                  <div>
                    <Image
                      src='/arrow 2.svg'
                      layout='fill'
                      objectFit='cover'
                      alt='arrow icon'
                    />
                  </div>
                </a>
              </Link>
            </li>
            <li>
              <Link href='/blog/topics/aries toolkit'>
                <a>
                  aries toolkit
                  <div>
                    <Image
                      src='/arrow 2.svg'
                      layout='fill'
                      objectFit='cover'
                      alt='arrow icon'
                    />
                  </div>
                </a>
              </Link>
            </li>
            <li>
              <Link href='/blog/topics/portfolio news'>
                <a>
                  portfolio news
                  <div>
                    <Image
                      src='/arrow 2.svg'
                      layout='fill'
                      objectFit='cover'
                      alt='arrow icon'
                    />
                  </div>
                </a>
              </Link>
            </li>
            <li>
              <Link href='/blog/topics/trend & insights'>
                <a>
                  trend & insights
                  <div>
                    <Image
                      src='/arrow 2.svg'
                      layout='fill'
                      objectFit='cover'
                      alt='arrow icon'
                    />
                  </div>
                </a>
              </Link>
            </li>
          </ul>
        </div>
      </div>
    </section>
  );
}
