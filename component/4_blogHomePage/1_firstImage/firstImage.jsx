import style from "./style.module.scss";
import Image from "next/image";

export default function FirstImage() {
  return (
    <section className={style.section}>
      <h1>
        <span>insight, resources</span> and stories.
      </h1>
      <div>
        <Image
          priority
          layout='fill'
          objectFit='cover'
          src='/blog page background.png'
          alt='background girl image'
        />
      </div>
    </section>
  );
}
