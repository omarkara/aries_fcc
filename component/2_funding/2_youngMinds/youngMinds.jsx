import style from "./style.module.scss";
import Image from "next/image";
import Link from "next/link";

export default function YoungMinds() {
  return (
    <section className={style.section}>
      <div className={style.topLeft}>
        <span>ROXERIN</span>
        <h2>Young Minds, Experience hand & Strong Command.</h2>
      </div>
      <div className={style.topRight}>
        <Image
          src='/stefan-cosma-GVlcXhQejA8-unsplash 1.png'
          layout='fill'
          objectFit='cover'
          alt='sun image'
        />
      </div>
      <div className={style.bottomLeft}>
        <p>
          We spoke to Robotic and sound designer Ben Lukas Boysen about the
          evolution of robot soundtracks and some of his top picks. The HAX
          program is designed to support pre-seed hard tech startups across all
          geographies and working in three focus areas : industrial, health and
          consumer.
        </p>
        <h3>how we worked together</h3>
        <p>
          Roxerin develops comprehensive hardware, software, content, and
          in-person events that integrate robotics, computer science, and
          education for students across the world.
        </p>
        <h3>YEAR OF FIRST INVESTMENT</h3>
        <p>2020</p>
        <Link href='/'>
          <a>
            company website
            <div>
              <Image
                src='/Arrow 2.svg'
                layout='fill'
                objectFit='cover'
                alt='Arrow'
              />
            </div>
          </a>
        </Link>
      </div>
      <div className={style.bottomRight}>
        <Image
          src='/Pattern.svg'
          layout='fill'
          objectFit='cover'
          alt='Pattern'
        />
      </div>
      <span className={style.line}></span>
      <span className={style.line1}></span>
      <span className={style.line2}></span>
    </section>
  );
}
