import style from "./style.module.scss";
import Image from "next/image";

export default function BackingFounders() {
  return (
    <section className={style.section}>
      <h1>
        <span>backing founders </span> that build big.
      </h1>
      <div>
        <Image
          priority
          layout='fill'
          objectFit='cover'
          src='/robot.png'
          alt='background girl image'
        />
      </div>
    </section>
  );
}
