import style from "./style.module.scss";
import Image from "next/image";
import Link from "next/link";

export default function Kawaski() {
  return (
    <section className={style.section}>
      <div className={style.images}>
        <div className={style.rightImage}>
          <Image
            src='/Rectangle 137.png'
            layout='fill'
            objectFit='cover'
            alt='background mone image'
          />
        </div>
        <div className={style.leftImage}>
          <Image
            src='/Rectangle 136.png'
            layout='fill'
            objectFit='cover'
            alt='robot'
          />
        </div>
      </div>
      <div className={style.textContent}>
        <span>KAWASKI</span>
        <h3>Corporobo Design Interview with Ben Lukas.</h3>
        <p>
          We spoke to Robotic and sound designer Ben Lukas Boysen about the
          evolution of robot soundtracks and some of his top picks.
        </p>
        <Link href='/'>
          <a>View project </a>
        </Link>
      </div>
    </section>
  );
}
