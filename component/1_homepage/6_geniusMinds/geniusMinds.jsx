import Image from "next/image";
import Link from "next/link";
import style from "./style.module.scss";

export default function GeniusMinds() {
  return (
    <section className={style.section}>
      <ul>
        <li>
          <div>
            <Image
              layout='fill'
              objectFit='cover'
              src='/Rectangle 146.png'
              alt='Edris Alba'
            />
          </div>
          <h3>Edris Alba</h3>
          <span>Leader</span>
        </li>
        <li>
          <div>
            <Image
              layout='fill'
              objectFit='cover'
              src='/Rectangle 144.png'
              alt='mark rafello'
            />
          </div>
          <h3>mark rafello</h3>
          <span>co - leader</span>
        </li>
        <li>
          <div>
            <Image
              layout='fill'
              objectFit='cover'
              src='/Rectangle 143.png'
              alt='chris rogers'
            />
          </div>
          <h3>chris rogers</h3>
          <span>lead marketing</span>
        </li>
        <li>
          <div>
            <Image
              layout='fill'
              objectFit='cover'
              src='/Rectangle 145.png'
              alt='chris rogers'
            />
          </div>
          <h3>roberto simioni</h3>
          <span>lead technician</span>
        </li>
      </ul>
      <span className={style.line}></span>
      <div>
        <span className={style.topA}>
          <Link href='/'>
            <a>View Team </a>
          </Link>
        </span>
        <h2>Genius minds behind this program for you</h2>
        <p>
          We are a group of engineers, designers and entrepreneur s brought
          together to support early stage startups working on artificial
          intelligence.
        </p>
        <span className={style.bottomA}>
          <Link href='/'>
            <a>View Team </a>
          </Link>
        </span>
      </div>
    </section>
  );
}
