import style from "./style.module.scss";
import Image from "next/image";
import Link from "next/link";

export default function Roxerin() {
  return (
    <section className={style.section}>
      <div className={style.images}>
        <div className={style.rightImage}>
          <Image
            src='/stefan-cosma-GVlcXhQejA8-unsplash 1.png'
            layout='fill'
            objectFit='cover'
            alt='background star image'
          />
        </div>
        <div className={style.leftImage}>
          <Image
            src='/maximalfocus-naSAHDWRNbQ-unsplash 1.png'
            layout='fill'
            objectFit='cover'
            alt='AI robot'
          />
        </div>
      </div>
      <div className={style.textContent}>
        <span>Roxerin</span>
        <h3>Young Minds, Experience hand & Strong Command.</h3>
        <p>
          We spoke to Robotic and sound designer Ben Lukas Boysen about the
          evolution of robot soundtracks and some of his top picks.
        </p>
        <Link href='/'>
          <a>View project </a>
        </Link>
      </div>
    </section>
  );
}
