import Link from "next/link";
import style from "./style.module.scss";
import { BsArrowRight } from "react-icons/bs";

export default function Portfolio() {
  return (
    <section className={style.section}>
      <span className={style.line}></span>
      <h2>latest portfolio jobs.</h2>
      <div className={style.content}>
        <ul className={style.portfolioJobs}>
          <li>
            <div>
              <span>KAWASKI</span>
              <h3>
                Sales Intern - <span> Right Hand to the COO</span>
              </h3>
              <p>Internship - Paris @agronavo - marketing</p>
            </div>
            <BsArrowRight />
          </li>
          <li>
            <div>
              <span>CYPHER</span>
              <h3>
                head of growth - <span>medical device startup</span>
              </h3>
              <p>ful time - remote - business develepoment</p>
            </div>
            <BsArrowRight />
          </li>
          <li>
            <div>
              <span>ORDIX</span>
              <h3>
                Software Developer - <span>Robot Systems</span>
              </h3>
              <p>ful time - remote - Software</p>
            </div>
            <BsArrowRight />
          </li>
        </ul>
        <div className={style.tags}>
          <h3>job categories</h3>
          <ul>
            <li>
              <Link href='/'>
                <a>Business development </a>
              </Link>
              <BsArrowRight />
            </li>
            <li>
              <Link href='/'>
                <a>Data Science </a>
              </Link>
              <BsArrowRight />
            </li>
            <li>
              <Link href='/'>
                <a>Finance </a>
              </Link>
              <BsArrowRight />
            </li>
            <li>
              <Link href='/'>
                <a>Human Resources</a>
              </Link>
              <BsArrowRight />
            </li>
            <li>
              <Link href='/'>
                <a>Operations </a>
              </Link>
              <BsArrowRight />
            </li>
            <li>
              <Link href='/'>
                <a>Software </a>
              </Link>
              <BsArrowRight />
            </li>
            <li>
              <Link href='/'>
                <a>Marketing </a>
              </Link>
              <BsArrowRight />
            </li>
          </ul>
        </div>
      </div>
      <div className={style.nextJobs}>
        <Link href='/'>
          <a>next jobs </a>
        </Link>
        <BsArrowRight />
      </div>
    </section>
  );
}
