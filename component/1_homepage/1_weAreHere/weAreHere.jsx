import style from "./style.module.scss";
import Image from "next/image";
export default function WeAreHere() {
  return (
    <section className={style.section} id='WeAreHere'>
      <h1>
        <span>We Are Here For Your </span>
        <span>Artificial Intelligence </span>
        <span>Startup.</span>
      </h1>
      <div>
        <Image
          src='/scroll arrow.svg'
          objectFit='contain'
          layout='fill'
          alt='circls background  '
        />
      </div>
    </section>
  );
}
