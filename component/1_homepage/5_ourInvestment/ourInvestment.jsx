import style from "./style.module.scss";
import line from "../../../styles/line.module.scss";

import Image from "next/image";

export default function OurInvestment() {
  return (
    <section className={style.section}>
      <span className={line.line}></span>
      <h2>Our Investment Portfolios</h2>
      <ul>
        <li>
          <div>
            <img src='/vector.svg' alt='partner svg logo' />
          </div>
          ATOMIN
        </li>
        <li>
          <div>
            <img src='/vector1.svg' alt='partner svg logo' />
          </div>
          oRACLE
        </li>
        <li>
          <div>
            <img src='/vector2.svg' alt='partner svg logo' />
          </div>
          JARVICE
        </li>
        <li>
          <div>
            <img src='/vector3.svg' alt='partner svg logo' />
          </div>
          RAXON
        </li>
        <li>
          <div>
            <img src='/vector4.svg' alt='partner svg logo' />
          </div>
          TINDO
        </li>
        <li>
          <div>
            <img src='/vector5.svg' alt='partner svg logo' />
          </div>
          KAWASKI
        </li>
        <li>
          <div>
            <img src='/vector6.svg' alt='partner svg logo' />
          </div>
          CYPHER
        </li>
        <li>
          <div>
            <img src='/vector7.svg' alt='partner svg logo' />
          </div>
          ORDIX
        </li>
      </ul>
    </section>
  );
}
