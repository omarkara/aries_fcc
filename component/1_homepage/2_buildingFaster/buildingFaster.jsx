import style from "./style.module.scss";
import Image from "next/image";
import Link from "next/link";

export default function BuildingFaster() {
  return (
    <section className={style.section} id='BuildingFaster'>
      <div className={style.rightImage}>
        <Image
          src='/big-centered image.png'
          layout='fill'
          objectFit='cover'
          alt='background girl image'
        />
      </div>
      <div className={style.leftImage}>
        <Image
          src='/small-right image (1).png'
          layout='fill'
          objectFit='cover'
          alt='programer'
        />
      </div>
      <div className={style.textContent}>
        <h2>building faster, smarter,together.</h2>
        <p>
          ARIES is a part of PIXIRHY, a global venture capital firm providing
          multi stage investment to develop and scale our founders’ big ideas.
          SOSV invests in every HAX program startup with follow-on through
          pre-iPO.
        </p>
        <Link href='/'>
          <a>View program </a>
        </Link>
      </div>
    </section>
  );
}
