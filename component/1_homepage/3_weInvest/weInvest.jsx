import style from "./style.module.scss";

export default function WeInvest() {
  return (
    <section className={style.section}>
      <h2>
        <span>We invest $27 Billion </span>
        <span>per year into AI startups.</span>
      </h2>
      <p>
        They are showing researches and writting regularly about the consequnces
        happening on AI. Our unique program is designed for startups, combining
        hands-on help from a deeply experienced product development team and a
        total investment package of $270,000.
      </p>
    </section>
  );
}
