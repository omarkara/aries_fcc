import Header from "../component/header/header";
import Custom404page from "../component/custom404page/custom404page";

export default function Custom404() {
  return (
    <>
      <Header />
      <Custom404page />
    </>
  );
}
